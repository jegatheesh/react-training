import todoDispatcher from './dispatcher';

export default class TodoAction {
  static addTodo = (data) => {
    todoDispatcher.dispatch({
      type: "ADD_TODO",
      data: data
    });
  } 

  static deleteTodo = (data) => {
    todoDispatcher.dispatch({
      type: "DELETE_TODO",
      data: data
    });
  } 
  static addTask = (data) => {
    todoDispatcher.dispatch({
      type: "ADD_TASK",
      data: data
    });
  } 
  static deleteTask = (data) => {
    todoDispatcher.dispatch({
      type: "DELETE_TASK",
      data: data
    });
  } 
  static openClickedTodo = (data) => {
    todoDispatcher.dispatch({
      type: "OPEN_TODO",
      data: data
    });
  } 
  static openClickedTask = (data) => {
    todoDispatcher.dispatch({
      type: "OPEN_TASK",
      data: data
    });
  } 
}  