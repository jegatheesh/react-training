import React, {Component} from 'react';

export default class InputField extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this);
    const {placeHolder, handleSubmit, name} = this.props;
    // const value = this.refs.name.valueOf();
    console.log(this);
    return (
      <form action="#" onSubmit={(a) => handleSubmit(a)}>
        <input ref={name} placeholder={placeHolder}/>
      </form>
    );
  }
}
