import React, {Component} from 'react';
import store from '../../store/store.js';
import TodoAction from '../../action.js';
import './scss/list.css'

class TodoContent extends Component {
  constructor(props) {
    super(props);
    this.state = {tasks: store.getActiveTodo().tasks, active: false};
  }

componentDidMount() {
  store.emitter.on('todo-updated', ()=> {this.rerender()});
}

componentWillUnmount() {
  store.emitter.removeListener('todo-updated', ()=> {this.rerender()});
}

handleMouseOver = e => {
  const task = store.getTaskById(e.target.parentNode.id);
  e.target.className = task.isChecked? "fa fa-check-circle" : "fa fa-check-circle-o";
}

handleMouseOut = e => {
  var task = store.getTaskById(e.target.parentNode.id);
  e.target.className = task.isChecked ? "fa fa-check-circle" : "fa fa-circle-thin";
}

handleCheckBoxClick = e => {
  e.stopPropagation();
  var task = store.getTaskById(e.target.parentNode.id);
  task.isChecked = !task.isChecked;
  this.rerender();
  console.log(this.state);
}

  generateChildren = () => {
     return (this.state.tasks.map((task) => {
       return (
         <li className="list-group-item" key={task.id} id={task.id} onDoubleClick={e => this.handleClick(e)} >
          <i className={ task.isChecked ? "fa fa-check-circle" : "fa fa-circle-thin"} onClick={e => this.handleCheckBoxClick(e)} onMouseOver={(e) => this.handleMouseOver(e)} onMouseOut={(e) => this.handleMouseOut(e)}></i>
          {task.name}
          <i className="fa fa-times delete-icon" onClick={task => this.deleteTask(task.id)}></i>
         </li>
       );
     }));
  }

  deleteTask = id => {
    TodoAction.deleteTask(id);
  }

  handleClick = e => {
    (e.target === e.currentTarget) &&
    TodoAction.openClickedTask(e.target.id);
  }

  rerender = () => {
    this.setState({tasks : store.getActiveTodo().tasks});
  }

  addTask = e => {
     e.preventDefault();
     const { taskName } = this.refs;
     TodoAction.addTask(taskName.value);
     this.refs.taskName.value = "";
  }

  render() {
    return (
      <div className="todo-content">
        <ul className="list-group">
          {this.generateChildren()}
        </ul>
         <form action="#" onSubmit={(e) => this.addTask(e)}>
          <input ref="taskName" placeholder="Add a task..."/>
        </form>
      </div>
    );
  }
}

export default TodoContent;
