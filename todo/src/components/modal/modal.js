import React from 'react';
import store from '../../store/store.js';
import TodoAction from '../../action.js';
import './scss/modal.css';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

export default class SubTodoContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      task: {
        name: "",
        id: "",
        notes: "",
        isChecked: false
      }
    };
  }

  componentDidMount() {
    store.emitter.on('changed-active-task', () => this.rerender());
  }

  componentWillUnmount() {
    store.emitter.removeListener('changed-active-task', () => this.rerender());
  }

  rerender = () => {
    this.setState({
      modal: !this.state.modal,
      task: store.getActiveTask()
    });
  }

  toggle = () => {
    this.state.modal && store.nullifyActiveTaskId();
    this.setState({
      modal: !this.state.modal,
      task: {
        name: "",
        id: "",
        notes: "",
        isChecked: false   
      }
    });
  }

  handleChange = e => {
    store.setNotes(e.target.value);
    const { modal } = this.state
    this.setState({
      modal,
      task: store.getActiveTask()
    });
  }

  deleteTodo = () => {
    var id = store.getActiveTaskId();
    this.toggle();
    TodoAction.deleteTask(id);
  }

  handleCheckBoxClick = e => {
    e.stopPropagation();
    const { task } = this.state;
    task.isChecked = !task.isChecked;
    this.setState({ task });
    store.setIsChecked();
  }

  render() {
    const { modal, task } = this.state;
    return (
      <div className="task-content">
        <Modal isOpen={modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>
            <i
              className={task.isChecked ? "fa fa-check-circle margin-rt-5" : "fa fa-circle-thin margin-rt-5"}
              onClick={e => this.handleCheckBoxClick(e)}
            />
            {task.name}
          </ModalHeader>
          <ModalBody>
            <textarea className="comments" value={task.notes} onChange={e => this.handleChange(e)}></textarea>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={() => this.deleteTodo()}>Delete</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
