import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './scss/navbar.css';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

export default class AppNavbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isCollapsed: false };
  }

  toggle = () => {
    this.setState({ isCollapsed: !this.state.isCollapsed });
  };

  render() {
    return (
      <div >
        <Navbar className="navbar-expand-lg  navigation-bar light" >
          <NavbarBrand href="/" className="black-elements">TODO</NavbarBrand>
          <NavbarToggler onClick={this.toggle} className="black-elements" />
          <Collapse isOpen={this.state.isCollapsed} className="flex-reverse" navbar>
            <Nav navbar>
              <NavItem className="float-rt">
                <NavLink href="#" className="black-elements">search</NavLink>
              </NavItem>
              <NavItem fixed="right">
                <NavLink href="#" className="black-elements">logout</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
