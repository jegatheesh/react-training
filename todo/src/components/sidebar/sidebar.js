import React from 'react';
import store from '../../store/store.js';
import TodoAction from '../../action.js';
import './scss/sidebar.css';

export default class AppSidebar extends React.Component {
  constructor(props) {
    super();
    this.state = { list: store.getTodos() };
    console.log(store.getTodos());
  }

  componentDidMount() {
    store.emitter.on('todo-added', this.rerender);
  }

  componentWillUnmount() {
    store.emitter.removeListener('todo-added', this.rerender);
  }

  rerender = () => {
    this.setState({ list: store.todos })
  }

  deleteTodo = e => {
    const { target } = e;
    e.stopPropagation();
    // store.deleteTodo(target.parentNode.id);
    TodoAction.deleteTodo(target.parentNode.id);
  }

  generateChildren = () => {
    console.log(store.getTodos());
    const deleteIcon = <i className="fa fa-times delete-icon" onClick={e => {this.deleteTodo(e)}} ></i>
    return this.state.list.map((todo) =>
      <li className="list-group-item" key={todo.id} id={todo.id} onClick={e => this.handleClick(e)}>
        <i className="fa fa-calendar todo-icon" />
        <span>{todo.name}</span>
        {todo.id !== 'default' && deleteIcon }
      </li>);
  }

  handleClick = e => {
    console.log("clicked elements id" +e.currentTarget.id);
    TodoAction.openClickedTodo(e.currentTarget.id);
  }

  addTodo = e => {
    TodoAction.addTodo(this.refs.todoName.value);
    //  store.addTodo(this.refs.todoName.value);
     this.refs.todoName.value = "";
  }

  render() {
      return (<div className="sidebar">
      <ul className="list-group">
        {this.generateChildren()}
      </ul>
      <form action="#" onSubmit={e => {this.addTodo(e)}}>
        <input ref="todoName" placeholder="Add a todo..."/>
      </form>
      </div>
      );
  }
}
