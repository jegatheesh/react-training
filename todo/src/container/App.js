import React, { Component } from 'react';
import AppNavbar from '../components/navbar/navbar.js';
import AppSidebar from '../components/sidebar/sidebar.js';
import TodoContent from '../components/list/content.js';
import SubTodoContent from '../components/modal/modal.js';
import './scss/App.css';

class App extends Component {
  render() {
    return (
      <div>
        <AppNavbar />
        <AppSidebar/>
        <TodoContent />
        <SubTodoContent />
      </div>
    );
  }
}

export default App;
