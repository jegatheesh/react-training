import EventEmitter from 'event-emitter';
import todoDispatcher from '../dispatcher';

 export default class store{
   static todos = [
                    {
                      name: "my day",
                      id: "default",
                      tasks:[
                        {
                          name: "task1",
                          id: "1",
                          isChecked: false,
                          notes: "qwertyuiop[asdfghjkl"
                        },
                        {
                          name: "myday-task",
                          id:"mydat-tasg",
                          isChecked: true,
                          notes:"asdsadasdsadsad"
                        }
                      ]
                    },
                    {
                      name: "todo",
                      id: "2",
                      tasks: [
                        {
                          name: "task2",
                          id: "task2",
                          isChecked: true,
                          notes: "asasqwertyuiopsdfghjkl"
                        }
                      ]
                    }
                  ];
  static activeTodoId = "default";
  static activeTaskId = "";
  static emitter = new EventEmitter();

  static getTodos = function(){
    return this.todos;
  }

  static setActiveTodoId = function(selectedTodoId) {
    this.activeTodoId = selectedTodoId;
    this.emitter.emit('todo-updated');
  }

  static setActiveTaskId = function(selectedTask) {
    this.activeTaskId = selectedTask;
    this.emitter.emit('changed-active-task');
  }

  static getActiveTodo = function() {
    return this.todos.find(todo => {
      return todo.id === this.activeTodoId;
    });
  }

  static getActiveTodoId = function() {
    return this.activeTodoId;
  }

  static getActiveTaskId = function() {
    return this.activeTaskId;
  }

  static addTodo = function(name) {
    this.todos.push({name: name, id: new Date().valueOf().toString(), isChecked: false,  tasks:[]});
    this.emitter.emit('todo-added');
  }

  static addTask = function(name) {
    console.log(this);
    this.getActiveTodo().tasks.push({name: name, id: new Date().valueOf().toString(), notes: ""});
    this.emitter.emit('todo-updated');
  }

  static getActiveTask = function() {
    return  this.getActiveTodo().tasks.find(task => {
      return task.id === this.activeTaskId;
    });
  }

  static getTodoById = function(todoId) {
    return this.todos.find(todo => {
      return todo.id === todoId;
    });
  }

  static getTaskById = function(taskId) {
    var tasks = this.getActiveTodo().tasks;
    return tasks.find(task => {
      return task.id === taskId;
    });
  }

  static setNotes = function(notes) {
    this.getActiveTask().notes = notes;
  }

  static deleteTodo = function(todoId) {
    if (todoId === this.activeTodoId) {
      this.setActiveTodoId('default');
    }
    this.todos.splice(this.todos.indexOf(this.getTodoById(todoId)), 1);
    this.emitter.emit('todo-added');
  }

  static deleteTask = function(taskId) {
    var activeTodo = this.getActiveTodo();
    activeTodo.tasks.splice(activeTodo.tasks.indexOf(this.getTaskById(taskId)), 1);
    this.emitter.emit('todo-updated');
  }

  static nullifyActiveTaskId = function() {
    this.activeTaskId = '';
  }

  static setIsChecked = function(taskId) {
    this.emitter.emit('todo-updated');
  }

  static handleAction = function(action) {
    switch(action.type) {
      case "ADD_TODO" :
        store.addTodo(action.data);
        break;
      case "ADD_TASK" :
        store.addTask(action.data);
        break;
      case "DELETE_TODO" :
        store.deleteTodo(action.data);
        break;
      case "DELETE_TASK" :
        store.deleteTask(action.data);
        break;
      case "OPEN_TODO" :
        store.setActiveTodoId(action.data);
        break;
      case "OPEN_TASK" :
        store.setActiveTaskId(action.data);
        break;
      default: 
        break;
    }
  }
}

todoDispatcher.register(store.handleAction);
